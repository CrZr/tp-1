# :memo: Compte Rendu Tp-1
## **| Self-Footprinting :footprints:** 
### **|| Host OS :**
- **Info system:**  
    - **Commande:** `systeminfo`

          Nom de l’hôte: MSIMANOA
    
          Nom du système d’exploitation: Microsoft Windows 10 Famille
    
          Version du système: 10.0.18363 N/A version 18363
        
          Type du système: x64-based PC
- **RAM:**   
    - **Commande:** `Get-WMIObject win32_physicalmemory`
        
          Capacity: 8589934592
        
    - **Commande:** `Get-CimInstance win32_physicalmemory | Format-Table Manufacturer, PartNumber`
    
          Fabricant et Modèle:
        
              SK Hynix 
            
              HMA81GS6DJR8N-VK 
-----------                -------------
### **||Devices :** 
- **Processeur:**
    - **Commande:** `Get-WmiObject win32_processor`
 
          Name : Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz
    - **Commande:** `wmic cpu get NumberOfLogicalProcessors/Format:List`

          NumberOfLogicalProcessors=12
    - **Commande:** `wmic cpu get NumberOfCores`
    
          NumberOfCores 6
 
 *"Il s'agit d'un processeur de 9ᵉ génération car le numéro 9 est indiqué après `i7`.
 Le `2.60GHz` est la fréquence de base du processeur. Le H indiqué après `i7-9750` signifie `High performance optimized for mobile`."*
 
 - **TouchPad:**

    - ***Commande:*** `Get-PnpDeviceProperty` 
    `HID\CUST0001&COL01\5&29DCE342&1&0000`
       
           HID\CUS... DEVPKEY_Device_DriverProvider String  Microsoft
           
           HID\CUS... DEVPKEY_Device_DriverVersion String  10.0.18362.1
- **Carte Graphique:**
    
    - ***Commande:*** `Get-WmiObject Win32_VideoController | Select description,driverversion`
        
          description: NVIDIA GeForce GTX 1660 Ti 
          
          driverversion: 25.21.14.2531

- **Disque Dur:**
    - ***Commande:*** `Get-disk | fl` 
    
          Manufacturer: -----
          
          Model: WDC PC SN520 SDAPNUW-512G-1032
          
    - ***Commande:*** `Get-Partition | fl`
          
          Partition Number 1
              Offset               : 1048576
              Size                 : 300 MB
              Type                 : System
              
        *"La partition **System** est la partition d'amorçage, elle permet de démarrer le système d'exploitation de l'ordinateur."*
          
          Partition Number 2
              Offset               : 315621376
              Size                 : 128 MB
              Type                 : Reserved
              
        *"La partition **Reserved** est généralement créé pendant le processus d'installation de Windows, juste avant que le programme d'installation n'alloue de l'espace pour la partition système principale."*
          
          Partition Number 3
              Offset               : 449839104
              Size                 : 457.9 GB
              Type                 : Basic
              
        *"La partition **Basic** est une partition de type générique pouvant être formatée en interne avec différents modes de formatage."*
        
          Partition Number 4 & 5
              Offset               : 492117688320
              Size                 : 900 MB
              Type                 : Recovery
              
              Offset               : 493061406720
              Size                 : 17.74 GB
              Type                 : Recovery
              
        *"Les partition **Recovery** sont des outils de logiciel avancé, destiné à tous les utilisateurs qui ont besoin de récupérer des données ou des partitions perdues."*
 -----------                -------------
 
 ### **|| Users :** 
 - **Users List:**
    
    - ***Commande:*** `wmic useraccount list full`
            
          User 1 : Administrateur
          
          User 2 : DefaultAccount
          
          User 3 : Invité
          
          User 4 : LDLC
          
          User 5 : WDAGUtilityAccount
          
    *"Le premier User est le compte grand administrateur intégré qui est désactivé par défaut, c'est celui qui est **full admin**, dans mon cas c'est l'User Administrateur "*
 -----------                -------------
### **|| Processus :**
- **5 Processus Elémentaire:**
    - ***Commande:*** `ps`
            
          dllhost.exe
        *"**dllhost.exe** est un processus du support de Microsoft Distributed composant Object Model , partie du système d'architecture des versions modernes de Windows et également quelques programmes faits par d'autres compagnies. En temps normal on l'utilise pour gérer les composants de programme de la Bibliothèque de liens dynamiques. C'est considéré comme le Principal système d'opération composant."*
          
          explorer.exe
        *"L'explorateur Windows (processus système **explorer.exe**) est un élément essentiel puisqu'il régit le bureau de Windows (Shell) avec le menu démarrer, la barre des tâches et le bureau avec le fond d'écran et les raccourcis en icônes."*
          
          winlogon.exe
        *"**winlogon.exe** est un processus qui réalise la fonction de gestion de connexion Windows, traitant la connexion de l'utilisateur et sa fermeture de session dans Windows (demande du nom d'utilisateur et votre mot de passe). Il est également responsable de charger les profils utilisateur après la connexion et de la surveillance de l'inactivité du clavier et de la souris pour décider quand mettre l'écran de veille."*
        
          rundll32.exe
        *"**rundll32.exe** est un des "chevaux de travail" des programmes du système d'opération sur Windows. Comme son nom le suggère, il est responsable de lancer les Bibliothèques de liens dynamiques (DLLs) et de les mettre en mémoire pour être utilisées par d'autres programmes. Cette version est responsable du code 32 bits, et elle est nécessaire parce que les DLLs ne peuvent pas s'exécuter directement tels des programmes."*
        
          searchindexer.exe
        *"Le processus **searchindexer.exe** correspond au service de recherche (ou service d'indexation) de Windows. Il s'agit d'un service chargé d'explorer le contenu du disque dur afin de faciliter la recherche d'un fichier sur ce dernier."*
        
***"Les processus lancés par l'utilisateur qui est full admin sont ceux avec un SI = 0 "***
 -----------                -------------
### **|| Network :**
- **Liste des Cartes Réseaux:**
    
    - ***Commande:*** `Get-NetAdapter | fl name`
    
          Name           : Wi-Fi
        *"La carte wifi est une norme de communication permettant la transmission de données numériques sans fil. Elle est appelée carte réseau compatible avec la norme WI-FI et équipée d’une antenne émettrice / réceptrice .Elle est également appelée Network Interface Card ( NIC ). Elle constitue l’Interface entre l’ordinateur et le câble du réseau. Sa fonction est de préparer , d’envoyer et de contrôler les données sur le réseau."*
        
          Name           : WSSVPNTap0901
        *"Il s'agit de carte réseau virtuelle qu'une application peut installer sur votre ordinateur. Généralement, il s'agit de logiciels de type VPN, Hamachi, etc. Ainsi chaque solution VPN (ExpressVPN, CyberGhost, NordVPN, PureVPN, etc) peut installer sa carte réseau virtuelle TAP. Lorsque le VPN s'active, le trafic réseau passe par cette carte virtuelle afin de chiffrer le contenu. Cette carte TAP sert donc de point d'entrée et sortie du tunnel VPN."*
        
          Name           : Ethernet
        *"Une carte réseau Ethernet sert d'interface physique entre l'ordinateur et le câble. Elle prépare pour le câble réseau les données émises par l'ordinateur, les transfère vers un autre ordinateur et contrôle le flux de données entre l'ordinateur et le câble. Elle traduit aussi les données venant du câble et les traduit en octets afin que l'Unité Centrale de l'ordinateur les comprenne. Ainsi une carte réseau est une carte d'extension s'insérant dans un connecteur d'extensions (slot)."*
- **Liste des ports TCP et UDP en Utilisation:**
    
    - ***Commande:*** `netstat -ano`

            Proto  Adresse locale         Adresse distante       État
            TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       1092
            RpcSs
            [svchost.exe]
            TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
            Impossible d’obtenir les informations de propriétaire
            TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING       4416
            CDPSvc
            [svchost.exe]
            TCP    0.0.0.0:6646           0.0.0.0:0              LISTENING       6288
            [MMSSHOST.EXE]
            TCP    0.0.0.0:7680           0.0.0.0:0              LISTENING       17556
            Impossible d’obtenir les informations de propriétaire
            TCP    0.0.0.0:9001           0.0.0.0:0              LISTENING       4
            Impossible d’obtenir les informations de propriétaire
            TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       536
            [lsass.exe]
            TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       996
            Impossible d’obtenir les informations de propriétaire
            TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1740
            EventLog
            [svchost.exe]
            TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       1484
            Schedule
            [svchost.exe]
            TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       4160
            [spoolsv.exe]
            TCP    0.0.0.0:49669          0.0.0.0:0              LISTENING       356
            Impossible d’obtenir les informations de propriétaire
            TCP    10.33.0.30:139         0.0.0.0:0              LISTENING       4
            Impossible d’obtenir les informations de propriétaire
            TCP    10.33.0.30:49676       40.67.254.36:443       ESTABLISHED     4780
            WpnService
            [svchost.exe]
            TCP    10.33.0.30:49741       23.57.5.23:443         CLOSE_WAIT      16372
            [WinStore.App.exe]
            TCP    10.33.0.30:49742       23.57.5.23:443         CLOSE_WAIT      16372
            [WinStore.App.exe]
            TCP    10.33.0.30:49746       23.57.5.23:443         CLOSE_WAIT      16372
            [WinStore.App.exe]
            TCP    10.33.0.30:49747       23.57.5.23:443         CLOSE_WAIT      16372
            [WinStore.App.exe]
            TCP    10.33.0.30:49751       23.57.5.23:443         CLOSE_WAIT      16372
            [WinStore.App.exe]
            TCP    10.33.0.30:50412       13.107.21.200:443      ESTABLISHED     8564
            [SearchUI.exe]
            TCP    10.33.0.30:50433       161.69.165.56:443      ESTABLISHED     9472
            [mcshield.exe]
            UDP    0.0.0.0:500            *:*                                    4704
            IKEEXT
            [svchost.exe]
            UDP    0.0.0.0:4500           *:*                                    4704
            IKEEXT
            [svchost.exe]
            UDP    0.0.0.0:5050           *:*                                    4416
            CDPSvc
            [svchost.exe]
            UDP    0.0.0.0:5353           *:*                                    2260
            Dnscache
            [svchost.exe]
            UDP    0.0.0.0:5355           *:*                                    2260
            Dnscache
            [svchost.exe]
            UDP    0.0.0.0:6646           *:*                                    6288
            [MMSSHOST.EXE]
            UDP    10.33.0.30:137         *:*                                    4
            Impossible d’obtenir les informations de propriétaire
            UDP    10.33.0.30:138         *:*                                    4
            Impossible d’obtenir les informations de propriétaire
            UDP    10.33.0.30:1900        *:*                                    10380
            SSDPSRV
            [svchost.exe]
            UDP    10.33.0.30:60625       *:*                                    10380
            SSDPSRV
            [svchost.exe]
            UDP    127.0.0.1:1900         *:*                                    10380
            SSDPSRV
            [svchost.exe]
            UDP    127.0.0.1:49664        *:*                                    5648
            iphlpsvc
            [svchost.exe]
            UDP    127.0.0.1:60626        *:*                                    10380
            SSDPSRV
            [svchost.exe]
            UDP    127.0.0.1:63394        *:*                                    2088
            NlaSvc
            [svchost.exe]
                  
    *"Derrière chaque port on peut associer un programme grâce à la dernière colonne de la liste qui renvoit le **PID** d'un processus"*

    [svchost.exe] *"**svchost.exe** est un des composants essentiels de Windows. Ce n'est pas une application à proprement parler, mais un processus, c'est à dire un programme sans interface tournant en arrière-plan. Son rôle principal consiste à charger des bibliothèques de liens dynamiques"*

    [MMSSHOST.EXE] *"McAfee Management Service Host appartient au logiciel McAfee de McAfee"*

    [lsass.exe] *"Il assure l'identification des utilisateurs."*

    [spoolsv.exe] *"**spoolsv.exe** est le principal service de file d'attente de l'imprimante – il s'occupe de gérer tout le travail d'impressions sur l'ordinateur. La file d'attente est un processus qui permet au système d'opération de mettre en file d'attente le travail d'impressions afin qu'il se fasse en arrière-plan, plutôt que de forcer l'utilisateur à attendre le temps que l'impression se termine."*

    [WinStore.App.exe] *"**WinStore.App.exe** est un fichier qui fait partie de l'écosystème du Windows Store et ne constitue pas une menace pour les utilisateurs PC. Le Microsoft Store est un point de distribution unifié pour les applications, la vidéo numérique, la musique numérique et les livres électroniques."*

    [SearchUI.exe] *"**SearchUI.exe** active l'interface utilisateur de recherche de l'assistant de recherche Microsoft Cortana."*

    [mcshield.exe] *"C'est un composant principal du produit d'analyse analyse de virus par l'antivirus de McAfee. Cela surveille les activités des autres processus, tels les accès aux fichiers et registre, afin d'essayer de détecter et de prévenir les infections par virus et autres logiciels malveillants."*

 -----------                -------------

## **| Gestion des Softs :**
### Chocolatey

Le gestionnaire de paquets est un système qui permet d'installer des logiciels, de les maintenir à jour et de les désinstaller. Son travail est de n'utiliser que des éléments compatibles entre eux, les installations sans utiliser de gestionnaire de paquets sont donc déconseillées.

### **|| List Soft :**
- ***Commande*** `choco list -l`
      
      Chocolatey v0.10.15
      chocolatey 0.10.15
      1 packages installed.

- ***Commande*** `choco source`
      
      Chocolatey v0.10.15
      chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.

## **| Machine Virtuelle :**

### **|| Configuration post-install :**

   *Afin de relié la VM et un PowerShell on procède ainsi*   

      PS C:\Users\LDLC> ssh root@192.168.120.50
      root@192.168.120.50's password:
      Last login: Mon Nov  9 17:07:10 2020
      [root@Death ~]# ip a
      1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
            valid_lft forever preferred_lft forever
      inet6 ::1/128 scope host
            valid_lft forever preferred_lft forever
      2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
      link/ether 08:00:27:d6:08:14 brd ff:ff:ff:ff:ff:ff
      inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
            valid_lft 85750sec preferred_lft 85750sec
      inet6 fe80::7394:9c6d:3f2e:faac/64 scope link noprefixroute
            valid_lft forever preferred_lft forever
      3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
      link/ether 08:00:27:b5:a0:60 brd ff:ff:ff:ff:ff:ff
      inet 192.168.120.50/24 brd 192.168.120.255 scope global noprefixroute dynamic enp0s8
            valid_lft 218sec preferred_lft 218sec
      inet6 fe80::cdab:ec55:e8bd:7ab/64 scope link noprefixroute
            valid_lft forever preferred_lft forever
      [root@Death ~]#

### **|| Partage de fichiers :**

 -----------                -------------