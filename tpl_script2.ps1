####################################################################################### 
#    Author: Manoa Marchand
#
#    Date: 24.10.2020
#
#    Comment: ce script peut effectuer des actions 
#    après un certain temps en fonction des arguments
####################################################################################### 

$2p=[String] $args[1]
$1p=[String] $args[0]

if ($1p -eq "lock") {

    Start-Sleep -Seconds $2p

    rundll32.exe user32.dll, LockWorkStationlok

} Elseif ($1p -eq "shutdown") {
    
    TIMEOUT $2p

    Shutdown /f /s /t 0
}