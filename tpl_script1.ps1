####################################################################################### 
#    Author: Manoa Marchand
#    Date: 24.10.2020
#    Comment: Script to obtain some information on this OS 
####################################################################################### 

# The variable

$name = hostname    # Nom de la machine

$env:HostIP = ( `
    Get-NetIPConfiguration | `
    Where-Object { `
        $_.IPv4DefaultGateway -ne $null `
        -and `
        $_.NetAdapter.Status -ne "Disconnected" `
    } `
).IPv4Address.IPAddress     # Adresse IP

$os = (Get-WmiObject -class Win32_OperatingSystem).Caption  # type de l'OS

$version=(Get-WmiObject Win32_OperatingSystem).Version  # Version de l'OS

$startTime = $(gcim Win32_OperatingSystem).LastBootUpTime   # Date et heure d'allumage

$osaj = [Environment]::OSVersion.Version -ge (new-object 'Version') # Checkupdate

$Taille_RAM_MAX=(Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory/1GB    # Taille totale de la ram

$Taille_RAM_LIBRE=(Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory/1MB   # Taille libre disponible de la ram

$Taille_RAM_UTILISE=$Taille_RAM_MAX - $Taille_RAM_LIBRE # Taille de la ram utilise

$Taille_Disk_Libre=(Get-wmiobject  Win32_LogicalDisk).Freespace/1GB # Taille du disque dur libre disponible

$Taille_Disk_UTILISE=(Get-wmiobject  Win32_LogicalDisk).Size/1GB - $Taille_Disk_Libre   # Taille du disque dur utilise

$rep_time = (Test-Connection -ComputerName "8.8.8.8" -Count 4  | measure-Object -Property ResponseTime -Average).average # Ping Moyen


Write-Host "============================="
Write-Host "OS Summary"

""

"Name : $name"
"IP Principal : $env:HostIP"
"OS : $os"
"Version : $version"
"Allumer depuis : $startTime"
"OS a jour ? : $osaj"
"RAM LIBRE : $Taille_RAM_LIBRE Go"
"RAM UTILISE : $Taille_RAM_UTILISE Go"
"DISK LIBRE : $Taille_DISK_Libre Go"
"DISK UTILISE : $Taille_DISK_UTILISE Go"

Write-Host "============================="
Write-Host "USERS"

""

(Get-LocalUser).Name

Write-Host "============================="
Write-Host "PING"

""

"Moyenne : $rep_time ms"

Write-Host "============================="